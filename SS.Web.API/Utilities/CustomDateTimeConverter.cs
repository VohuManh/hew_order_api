﻿//using Newtonsoft.Json.Converters;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Newtonsoft.Json;
//using System.Web;
//using SS.Core;

//namespace SS.Web.API.Utilities
//{
//    public class CustomDateTimeConverter : IsoDateTimeConverter
//    {
//        List<string> Formats = new List<string>();

//        public CustomDateTimeConverter(List<string> formats)
//        {
//            Formats = formats;
//            DateTimeFormat = formats.FirstOrDefault();
//            Culture = new CultureInfo("en-US");
//        }

//        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
//        {
//            string timeZone = HttpContextProvider.Current.Request.Headers["Timezone"];

//            foreach (string format in Formats)
//            {
//                if ((reader.ValueType == typeof(string)) && (reader.Value.ToDate(format, Culture) != null))
//                {
//                    DateTime value = reader.Value.ToDate(format, Culture).GetValueOrDefault();
//                    if ((format != DateFormatter.ISODateFormat) && !string.IsNullOrWhiteSpace(timeZone))
//                    {
//                        string timeoperator = timeZone.Substring(0, 1);
//                        int timeZoneOffset = timeZone.Substring(1, timeZone.Length - 1).ToInteger(0);

//                        if(timeoperator == "+")
//                        {
//                            value = value.AddHours(-timeZoneOffset);
//                        }
//                        else
//                        {
//                            value = value.AddHours(timeZoneOffset);
//                        }
//                    }
//                    return value;
//                }
//                else if(reader.ValueType == typeof(DateTime))
//                {
//                    DateTime value = (DateTime)reader.Value;
//                    if ((format != DateFormatter.ISODateFormat) && !string.IsNullOrWhiteSpace(timeZone))
//                    {
//                        string timeoperator = timeZone.Substring(0, 1);
//                        int timeZoneOffset = timeZone.Substring(1, timeZone.Length - 1).ToInteger(0);

//                        if (timeoperator == "+")
//                        {
//                            value = value.AddHours(-timeZoneOffset);
//                        }
//                        else
//                        {
//                            value = value.AddHours(timeZoneOffset);
//                        }
//                    }
//                    return value;
//                }
//            }

//            if(objectType == typeof(DateTime?))
//            {
//                return null;
//            }
//            else
//            {
//                return default(DateTime);
//            }
//        }

//        public override void WriteJson(Newtonsoft.Json.JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
//        {
//            string timeZone = HttpContextProvider.Current.Request.Headers["Timezone"];

//            if (!string.IsNullOrWhiteSpace(timeZone))
//            {
//                DateTime responseValue = (DateTime)value;
//                string timeoperator = timeZone.Substring(0, 1);
//                int timeZoneOffset = timeZone.Substring(1, timeZone.Length - 1).ToInteger(0);

//                if (timeoperator == "+")
//                {
//                    responseValue = responseValue.AddHours(timeZoneOffset);
//                }
//                else
//                {
//                    responseValue = responseValue.AddHours(-timeZoneOffset);
//                }

//                writer.WriteValue(responseValue.ToDateString(this.DateTimeFormat, Culture));
//            }
//            else
//            {
//                writer.WriteValue(value.ToDateString(this.DateTimeFormat, Culture));
//            }
//        }
//    }
//}
