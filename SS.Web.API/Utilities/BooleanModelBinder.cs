﻿//using Microsoft.AspNetCore.Mvc.ModelBinding;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.Http.Controllers;
//using System.Web.Http.ModelBinding;

//namespace SS.Web.API.Utilities
//{
//    public class BooleanModelBinder : IModelBinder
//    {
//        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
//        {
//            var valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
//            if (valueResult == null || string.IsNullOrEmpty(valueResult.AttemptedValue))
//            {
//                bindingContext.Model = false;
//                return false;
//            }

//            var modelState = new ModelState { Value = valueResult };
//            bool actualValue = false;
//            try
//            {
//                actualValue = valueResult.AttemptedValue.ToBoolean().GetValueOrDefault();
//            }
//            catch (FormatException e)
//            {
//                modelState.Errors.Add(e);
//            }

//            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
//            bindingContext.Model = actualValue;

//            return true;
//        }
//    }
//}
