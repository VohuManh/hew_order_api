﻿//using SS.Core;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.Http.Controllers;
//using System.Web.Http.ModelBinding;

//namespace SS.Web.API.Utilities
//{
//    public class DateTimeModelBinder : IModelBinder
//    {
//        private static CultureInfo Culture = new CultureInfo("en-US");
//        private List<string> Formats = new List<string>();

//        public DateTimeModelBinder(List<string> formats)
//        {
//            Formats = formats;
//        }

//        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
//        {
//            var valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
//            if (valueResult == null || string.IsNullOrEmpty(valueResult.AttemptedValue))
//            {
//                bindingContext.Model = default(DateTime);
//                return true;
//            }

//            var modelState = new ModelState { Value = valueResult };
//            DateTime actualValue = default(DateTime);
//            try
//            {
//                string timeZone = HttpContextProvider.Current.Request.Headers["Timezone"];

//                foreach (string format in Formats)
//                {
//                    DateTime? value = valueResult.AttemptedValue.ToDate(format, Culture);
//                    if ((format != DateFormatter.ISODateFormat) && !string.IsNullOrWhiteSpace(timeZone) && value.HasValue)
//                    {
//                        string timeoperator = timeZone.Substring(0, 1);
//                        int timeZoneOffset = timeZone.Substring(1, timeZone.Length - 1).ToInteger(0);

//                        if (timeoperator == "+")
//                        {
//                            value = value?.AddHours(-timeZoneOffset);
//                        }
//                        else
//                        {
//                            value = value?.AddHours(timeZoneOffset);
//                        }
//                    }

//                    if(value.HasValue)
//                    {
//                        actualValue = value.GetValueOrDefault();
//                        break;
//                    }
//                }
//            }
//            catch (FormatException e)
//            {
//                modelState.Errors.Add(e);
//            }

//            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
//            bindingContext.Model = actualValue;

//            return true;
//        }
        
//    }
//}
