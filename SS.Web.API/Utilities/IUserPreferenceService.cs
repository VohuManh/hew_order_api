﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS.Web.API.Utilities
{
    public interface IUserPreferenceService
    {
        void UpdateUserLanguage(int userId, string lang);
        void UpdateUserTimeZone(int userId, int timeZone);
        string GetUserLanguage(int userId);
        int GetUserTimeZone(int userId);
    }
}
