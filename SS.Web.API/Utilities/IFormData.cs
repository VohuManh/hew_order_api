﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SS.Web.API.Utilities
{
    public interface IFormData
    {
        string GetFormData(string key);
        void PutData(string key, string value);

        Dictionary<string, IFormFile> GetFiles();
        T GetFormDataMapping<T>();
        void PutFile(string key, byte[] data, string filename, string contentType);
    }
}
