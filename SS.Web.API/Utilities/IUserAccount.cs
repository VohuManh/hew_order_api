﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS.Web.API.Utilities
{
    public interface IUserAccount
    {
        int UserID { get; }
        string FirstName { get; }
        string LastName { get; }
        string Email { get; }
        List<string> Roles { get; }
        string ClientId { get; }
        string CurrentProgram { get; }
    }
}
