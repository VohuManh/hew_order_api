﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SS.Web.API.Utilities
{
    public static class ServiceProvider
    {
        public static Microsoft.Extensions.DependencyInjection.ServiceProvider Provider { get; set; }

        public static IUserAccount UserAccountService
        {
            get
            {
                return Provider.GetService(typeof(IUserAccount)) as IUserAccount;
            }
        }
    }
}
