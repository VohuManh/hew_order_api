﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NLog;
using SS.Validation.Exceptions;
using SS.Web.API.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SS.Web.API.Filter
{
    public class ExceptionHandlingFilter : ExceptionFilterAttribute
    {
        public static Logger logger = LogManager.GetLogger("fileError");
     
        public override void OnException(ExceptionContext context)
        {
            HttpResponse response = context.HttpContext.Response;
            List<string> errorMessages = null;

            if (context.Exception is ServiceException)
            {
                errorMessages = (context.Exception as ServiceException).ValidationErrors;

                if (errorMessages != null && errorMessages.Count > 0)
                {
                    errorMessages = errorMessages.Distinct().ToList();
                }

                response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result = new JsonResult(new BaseResponse() { IsSuccess = false, ErrorMessages = errorMessages });
            }
            else if (context.Exception.InnerException != null && context.Exception.InnerException is ServiceException)
            {
                errorMessages = (context.Exception.InnerException as ServiceException).ValidationErrors;

                if (errorMessages != null && errorMessages.Count > 0)
                {
                    errorMessages = errorMessages.Distinct().ToList();
                }

                response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result = new JsonResult(new BaseResponse() { IsSuccess = false, ErrorMessages = errorMessages });
            }

            if (context != null && context.Exception != null)
            {
               logger.Error(context.Exception.ToString());
            }
        }
    }
}
