﻿//using NLog;
//using SS.Data.Caching;
//using SS.Web.API.Utilities;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Runtime.Caching;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web.Http.Controllers;
//using System.Web.Http.Filters;
//using Unity;

//namespace SS.Web.API.Filter
//{
//    public class UserPreferenceFilter : ActionFilterAttribute
//    {
//        public static Logger logger = LogManager.GetLogger("fileError");
//        public static IUnityContainer Container { get; set; }
//        public static CacheItemPolicy Policy = new CacheItemPolicy()
//        {
//            SlidingExpiration = TimeSpan.FromMinutes(60)
//        };

//        public static IUserPreferenceService UserPreferenceService
//        {
//            get
//            {
//                if (Container != null)
//                {
//                    return Container.Resolve<IUserPreferenceService>();
//                }
//                return null;
//            }
//        }

//        public override void OnActionExecuting(HttpActionContext actionContext)
//        {
//            if (UserAccountService.CurrentUserAccount != null && UserAccountService.CurrentUserAccount.UserId > 0)
//            {
//                string key = UserAccountService.CurrentUserAccount.UserId.ToString();

//                //User language
//                if (!CacheProvider.UserLanguageCache.Contains(key))
//                {
//                    string language = UserPreferenceService.GetUserLanguage(UserAccountService.CurrentUserAccount.UserId);
//                    CacheProvider.UserLanguageCache.Add(new System.Runtime.Caching.CacheItem(key, language), Policy);
//                }

//                if (CacheProvider.UserLanguageCache.Contains(key) && CacheProvider.UserLanguageCache[key].ToString() != CultureInfo.CurrentCulture.Name)
//                {
//                    UserPreferenceService.UpdateUserLanguage(UserAccountService.CurrentUserAccount.UserId, CultureInfo.CurrentCulture.Name);
//                    CacheProvider.UserLanguageCache[key] = CultureInfo.CurrentCulture.Name;
//                }

//                string timeZone = HttpContextProvider.Current.Request.Headers["Timezone"];
//                if(!string.IsNullOrEmpty(timeZone))
//                {
//                    //User timezone
//                    if (!CacheProvider.UserTimezoneCache.Contains(key))
//                    {
//                        int oldtimezone = UserPreferenceService.GetUserTimeZone(UserAccountService.CurrentUserAccount.UserId);
//                        CacheProvider.UserTimezoneCache.Add(new System.Runtime.Caching.CacheItem(key, oldtimezone), Policy);
//                    }

//                    if (CacheProvider.UserTimezoneCache.Contains(key) && CacheProvider.UserTimezoneCache[key].ToInteger(0) != timeZone.ToInteger(0))
//                    {
//                        UserPreferenceService.UpdateUserTimeZone(UserAccountService.CurrentUserAccount.UserId, timeZone.ToInteger(0));
//                        string old = CacheProvider.UserTimezoneCache[key].ToString();
//                        CacheProvider.UserTimezoneCache[key] = timeZone.ToInteger(0);
//                        logger.Error("Time zone change for: User " + UserAccountService.CurrentUserAccount.UserId + " from " + old + " to " + timeZone + "(from " + actionContext.Request.RequestUri + ")");
//                    }
//                }
//            }
//        }
//    }
//}
