﻿using Microsoft.AspNetCore.Mvc.Filters;
using NLog;
using SS.Web.API.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS.Web.API.Filter
{
    public class LoggingRequestFilter : ActionFilterAttribute
    {
        public static Logger logger = LogManager.GetLogger("requestLog");

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            logger.Info(string.Format("UserId: {0} \n RequestOn: {1}", ServiceProvider.UserAccountService.UserID, actionContext.HttpContext.Request.Path.ToString()));
            if (actionContext.ActionArguments != null && actionContext.ActionArguments.Count > 0)
            {
                foreach (var param in actionContext.ActionArguments)
                {
                    if (param.Value != null)
                    {
                        logger.Info(string.Format("Parameter for: {0} is {1}", param.Key, Newtonsoft.Json.JsonConvert.SerializeObject(param.Value)));
                    }
                    else
                    {
                        logger.Info(string.Format("Parameter for: {0} is null", param.Key));
                    }
                }

            }
        }

        //        public async Task<string> BlaAsync(HttpActionContext actionContext)
        //        {
        //            var sss = await actionContext.Request.Content.ReadAsStringAsync();
        //            return sss;
        //        }


    }
}
