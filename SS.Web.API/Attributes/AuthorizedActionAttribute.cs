﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SS.Web.API.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SS.Web.API.Attributes
{
    public class AuthorizedActionAttribute : ActionFilterAttribute
    {
        public bool AllowAnnonymous { get; set; }
        public string Roles { get; set; }

        public AuthorizedActionAttribute(params string[] roles)
        {
            this.Roles = string.Join(",", roles);
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            var otherAttributes = actionContext.ActionDescriptor.FilterDescriptors.Where(x => x.Filter is AuthorizedActionAttribute).ToList();
            AuthorizedActionAttribute attributes = null;
            List<string> roles = new List<string>();

            if (otherAttributes != null && otherAttributes.Count > 0)
            {
                attributes = ((AuthorizedActionAttribute)otherAttributes[0].Filter);
                roles = attributes.Roles.Split(',').Select(y => y.Trim()).ToList();
            }

            if ((roles != null && (ServiceProvider.UserAccountService.Roles == null || !ServiceProvider.UserAccountService.Roles.Any(x => roles.Contains(x))))
                && (!attributes.AllowAnnonymous)
               )
            {
                actionContext.Result = new UnauthorizedResult();
            }
            base.OnActionExecuting(actionContext);
        }
    }
}
