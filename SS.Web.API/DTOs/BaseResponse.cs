﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SS.Web.API.DTOs
{
    public class BaseResponse 
    {
        private bool isSuccess = true;
        public bool IsSuccess
        {
            get
            {
                return isSuccess;
            }
            set
            {
                isSuccess = value;
            }
        }
        public List<string> ErrorMessages { get; set; }
    }
}
