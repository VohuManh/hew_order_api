﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Hiw.BL;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using Hiw.Api.Models;
using SS.Web.API.Utilities;
using SS.Web.API.Filter;
using Microsoft.IdentityModel.Logging;
using IdentityServer4;

namespace Hiw.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        readonly string AllowSpecificOrigins = "AllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(AllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins(ConfigurationHelper.Configuration.GetSection("StartupConfig:CorsAllowClient").Value.Split(","));
                    builder.AllowCredentials();
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });
            });

            services.AddMvc(config =>
                    {
                        config.Filters.Add(typeof(ExceptionHandlingFilter));
                        config.Filters.Add(typeof(LoggingRequestFilter));
                    })
                    .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                    .AddJsonOptions(o =>
                    {
                        o.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    })
                    .AddFluentValidation(fv =>
                    {
                        fv.RegisterValidatorsFromAssemblies(new List<Assembly>() { typeof(BL.Services.ServiceProvider).Assembly });
                        fv.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                    });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = IdentityServerAuthenticationDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                // base-address of your identityserver
                options.Authority = ConfigurationHelper.Configuration.GetSection("StartupConfig:IdentityserverUrl").Value;
                // name of the API resource
                options.Audience = ConfigurationHelper.Configuration.GetSection("StartupConfig:ApiName").Value;
            });

            //Google Authentication by identityserver
           // .AddGoogle("Google", options =>
           //{
           //    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;

           //    options.ClientId = "648780662070-paamofsnca28o47u1qm2c3c7u4aiak99.apps.googleusercontent.com";
           //    options.ClientSecret = "B3wozU_IXyWhcCu5HnFn6hVy";
               

           //});

            // create service provider
            services.AddScoped<IUserAccount, UserAccount>();
            services.AddScoped<IFormData, FormData>();

            var serviceProvider = services.BuildServiceProvider();
            BL.Services.ServiceProvider.Provider = serviceProvider;
            SS.Web.API.Utilities.ServiceProvider.Provider = serviceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            IdentityModelEventSource.ShowPII = true;
            app.UseCors(AllowSpecificOrigins);
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
