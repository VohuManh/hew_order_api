﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Hiw.BL.DTOs.Responses;
using Hiw.BL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SS.Validation.Exceptions;
using SS.Web.API.Attributes;

namespace Hiw.Api.Controllers
{
    public class TestModel
    {
        public int TestInt1 { get; set; }
        public decimal TestDecimal1 { get; set; }
        public string TestString { get; set; }
        public bool Testbool1 { get; set; }
    }

    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        [Authorize]
        //[AuthorizedAction (Roles = "1,2,3")]
        public ActionResult Get()
        {
            var UserID = ServiceProvider.UserAccountService.UserID;
            var CurrentProgram = ServiceProvider.UserAccountService.CurrentProgram;

            //var data = JsonConvert.DeserializeObject<TestModel>(ServiceProvider.FormDataService.GetFormData("Data")?.ToString());
            //var files = ServiceProvider.FormDataService.GetFiles();

            //foreach (string item in files.Keys)
            //{
            //    using (var reader = new BinaryReader(files[item].OpenReadStream()))
            //    {
            //        var FileByte = reader.ReadBytes(files[item].Length.ToInteger(0));
            //    }
            //    break;
            //}

            //ItemResponse<TestModel> res = new ItemResponse<TestModel>();
            //res.Data = data;

            //throw new ServiceException(new string[] { "aaaaaaa" });

            return Ok(UserID);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
