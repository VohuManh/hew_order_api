﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Hiw_Order.BL.Entities.So;
using Hiw.BL.Services;

namespace Hiw_Order.Api.Controllers
{
    [Route("api/order")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        [Route("post")]
        [HttpPost]
        public int PostOrder([FromBody]SoOrder soOrder)
        {

            var orderID = ServiceProvider.SoOrderService.InsertOrder(soOrder);
            return 1;
        }
    }
}