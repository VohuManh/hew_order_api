﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hiw.BL.DTOs.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SS.Web.API.Controller;

namespace Hiw.Api.Controllers
{
    [Route("api/SystemConfiguration")]
    [ApiController]
    public class SystemConfigurationController : BaseApiController
    {
        [HttpGet]
        [Route("GetClientSystemConfiguration")]
        public ActionResult GetClientSystemConfiguration()
        {
            // ItemResponse<List<SuSystemConfiguration>> res = new ItemResponse<List<SuSystemConfiguration>>();
            // res.Data = ServiceProvider.SuSystemConfigurationService.GetAllSystemConfiguration();

            return Ok();
        }
    }
}
