﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Hiw.BL.Services;
using Hiw_Order.BL.Entities.Su;
using Hiw_Order.BL.Entities.Db;


namespace Hiw_Order.Api.Controllers
{
    [Route("api/address")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        [Route("getProvince")]
        [HttpGet]
        public IEnumerable<DbProvince> GetProvince()
        {
            return ServiceProvider.DbProvinceService.GetProvinces();
        }

        [Route("getDistrict/{provinceCode}")]
        [HttpGet]
        public IEnumerable<DbDistrict> GetDistricts(int provinceCode)
        {
            return ServiceProvider.DbDistrictService.GetDistrict(provinceCode);
        }

        [Route("getCanton/{districtCode}")]
        [HttpGet]
        public IEnumerable<DbCanton> GetCantons(int districtCode)
        {
            return ServiceProvider.DbCantonService.GetCanton(districtCode);
        }

        [Route("getPostCode/{cantonCode}")]
        [HttpGet]
        public IEnumerable<DbPostCode> GetPostCodes(int cantonCode)
        {
            return ServiceProvider.DbPostCodeService.GetPostCode(cantonCode);
        }

        [Route("update")]
        [HttpPatch]
        public ActionResult UpdateAddress([FromBody] SuAddress suAddress)
        {
            var address = ServiceProvider.SuAddressService.UpdateAddress(suAddress ,out string errorMessage);

            return Ok(address);
        }

        [Route("insert")]
        [HttpPost]
        public ActionResult Post([FromBody] SuAddress suAddress)
        {
           var id = ServiceProvider.SuAddressService.InsertAddress(suAddress);

           return Ok(id);
        }
    }
}