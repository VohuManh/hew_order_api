﻿using Hiw.BL;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SS.Web.API.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Hiw.Api.Models
{
    public class UserAccount : IUserAccount
    {
        private readonly IHttpContextAccessor context;

        public UserAccount(IHttpContextAccessor context)
        {
            this.context = context;
        }

        public int UserID
        {
            get
            {
                if (context.HttpContext.User != null && context.HttpContext.User.Identity != null)
                {
                    var claims = (context.HttpContext.User.Identity as ClaimsIdentity);
                    if (claims != null)
                    {
                        Claim claim = claims.Claims.FirstOrDefault(x => x.Type == CustomJwtClaimTypes.UserID);
                        if (claim != null)
                        {
                            return claim.Value.ToInteger(0);
                        }
                    }
                }
                return 0;
            }
        }

        public string FirstName
        {
            get
            {
                if (context.HttpContext.User != null && context.HttpContext.User.Identity != null)
                {
                    var claims = (context.HttpContext.User.Identity as ClaimsIdentity);
                    if (claims != null)
                    {
                        Claim claim = claims.Claims.FirstOrDefault(x => x.Type == CustomJwtClaimTypes.FirstName);
                        if (claim != null)
                        {
                            return claim.Value;
                        }
                    }
                }
                return string.Empty;
            }
        }

        public string LastName
        {
            get
            {
                if (context.HttpContext.User != null && context.HttpContext.User.Identity != null)
                {
                    var claims = (context.HttpContext.User.Identity as ClaimsIdentity);
                    if (claims != null)
                    {
                        Claim claim = claims.Claims.FirstOrDefault(x => x.Type == CustomJwtClaimTypes.LastName);
                        if (claim != null)
                        {
                            return claim.Value;
                        }
                    }
                }
                return string.Empty;
            }
        }

        public string FullName
        {
            get
            {
                if (context.HttpContext.User != null && context.HttpContext.User.Identity != null)
                {
                    var claims = (context.HttpContext.User.Identity as ClaimsIdentity);
                    if (claims != null)
                    {
                        Claim claim = claims.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Name);
                        if (claim != null)
                        {
                            return claim.Value;
                        }
                    }
                }
                return string.Empty;
            }
        }

        public string Email
        {
            get
            {
                if (context.HttpContext.User != null && context.HttpContext.User.Identity != null)
                {
                    var claims = (context.HttpContext.User.Identity as ClaimsIdentity);
                    if (claims != null)
                    {
                        Claim claim = claims.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Email);
                        if (claim != null)
                        {
                            return claim.Value;
                        }
                    }
                }
                return string.Empty;
            }
        }

        public List<string> Roles
        {
            get
            {
                if (context.HttpContext.User != null && context.HttpContext.User.Identity != null)
                {
                    var claims = (context.HttpContext.User.Identity as ClaimsIdentity);
                    if (claims != null)
                    {
                        Claim claim = claims.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Role);
                        if (claim != null)
                        {
                            return JsonConvert.DeserializeObject<List<string>>(claim.Value);
                        }
                    }
                }
                return null;
            }
        }

        public string ClientId
        {
            get
            {
                if (context.HttpContext.User != null && context.HttpContext.User.Identity != null)
                {
                    var claims = (context.HttpContext.User.Identity as ClaimsIdentity);
                    if (claims != null)
                    {
                        Claim claim = claims.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.ClientId);
                        if (claim != null)
                        {
                            return claim.Value;
                        }
                    }
                }
                return string.Empty;
            }
        }

        public string CurrentProgram
        {
            get
            {
                if (context.HttpContext.Request != null && context.HttpContext.Request.Headers.Keys.Contains("Current-Program"))
                {
                    return context.HttpContext.Request.Headers["Current-Program"];
                }
                return string.Empty;
            }
        }

    }
}
