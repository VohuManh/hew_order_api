﻿using Microsoft.AspNetCore.Http;
using SS.Web.API.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SS.Core.Converters;
using System.Reflection;
using SS.Core;

namespace Hiw.Api.Models
{
    public class FormData : IFormData
    {
        private readonly IHttpContextAccessor context;

        public FormData(IHttpContextAccessor context)
        {
            this.context = context;
        }

        public string GetFormData(string key)
        {
            return context.HttpContext.Request.Form[key].ToString();
        }

        public T GetFormDataMapping<T>()
        {
            var result = (T)Activator.CreateInstance(typeof(T));
            var properties = typeof(T).GetProperties();

            foreach (PropertyInfo info in properties)
            {
                if (!string.IsNullOrEmpty(context.HttpContext.Request.Form[info.Name]))
                {
                    if (info.PropertyType == typeof(int))
                    {
                        info.SetValue(result, context.HttpContext.Request.Form[info.Name].ToInteger(0));
                    }
                    else if (info.PropertyType == typeof(int?))
                    {
                        info.SetValue(result, context.HttpContext.Request.Form[info.Name].ToInteger());
                    }
                    else if (info.PropertyType == typeof(decimal))
                    {
                        info.SetValue(result, context.HttpContext.Request.Form[info.Name].ToDecimal(0));
                    }
                    else if (info.PropertyType == typeof(decimal?))
                    {
                        info.SetValue(result, context.HttpContext.Request.Form[info.Name].ToDecimal());
                    }
                    else if (info.PropertyType == typeof(string))
                    {
                        info.SetValue(result, context.HttpContext.Request.Form[info.Name]);
                    }
                    else if (info.PropertyType == typeof(bool))
                    {
                        if (context.HttpContext.Request.Form[info.Name] == "0")
                        {
                            info.SetValue(result, false);
                        }
                        else if (context.HttpContext.Request.Form[info.Name] == "1")
                        {
                            info.SetValue(result, true);
                        }
                        else
                        {
                            info.SetValue(result, context.HttpContext.Request.Form[info.Name].ToBoolean(false));
                        }
                    }
                    else if (info.PropertyType == typeof(bool?))
                    {
                        if (context.HttpContext.Request.Form[info.Name] == "0")
                        {
                            info.SetValue(result, false);
                        }
                        else if (context.HttpContext.Request.Form[info.Name] == "1")
                        {
                            info.SetValue(result, true);
                        }
                        else
                        {
                            info.SetValue(result, context.HttpContext.Request.Form[info.Name].ToBoolean());
                        }
                    }
                    else if (info.PropertyType == typeof(DateTime))
                    {
                        var data = context.HttpContext.Request.Form[info.Name].ToDate(DateFormatter.ISODateTimeFormat);
                        if (data == null)
                        {
                            data = context.HttpContext.Request.Form[info.Name].ToDate(DateFormatter.ISODateFormat);
                        }
                        info.SetValue(result, data.GetValueOrDefault());
                    }
                    else if (info.PropertyType == typeof(DateTime?))
                    {
                        var data = context.HttpContext.Request.Form[info.Name].ToDate(DateFormatter.ISODateTimeFormat);
                        if (data == null)
                        {
                            data = context.HttpContext.Request.Form[info.Name].ToDate(DateFormatter.ISODateFormat);
                        }
                        info.SetValue(result, data);
                    }

                }
            }

            return result;
        }

        public void PutData(string key, string value)
        {

        }

        public Dictionary<string, IFormFile> GetFiles()
        {
            Dictionary<string, IFormFile> files = new Dictionary<string, IFormFile>();
            foreach (var file in context.HttpContext.Request.Form.Files)
            {
                files[file.Name] = file;
            }
            return files;
        }

        public void PutFile(string key, byte[] data, string filename, string contentType)
        {

        }
    }
}
