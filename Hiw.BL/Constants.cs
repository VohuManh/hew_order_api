﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiw.BL
{
    public struct CustomJwtClaimTypes
    {
        public const string UserID = "userid";
        public const string FirstName = "firstName";
        public const string LastName = "lastName";
    }

    public struct CustomJwtClaimTypesEmail
    {
        public const string Email = "email";
        public const string Expired = "expired";
    }
}
