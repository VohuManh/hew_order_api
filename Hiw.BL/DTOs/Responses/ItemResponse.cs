﻿using SS.Web.API.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hiw.BL.DTOs.Responses
{
    public class ItemResponse<T> : BaseResponse
    {
        public T Data { get; set; }
        public int TotalCount { get; set; }
    }
}
