﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using Newtonsoft.Json;
using Hiw.BL.Services.Rabbit.Function;

namespace Hiw.BL.Services.Rabbit
{
   public class Sender
    {

        public class AddData
        {
            public string Username { get; set; }

            public string Shipping { get; set; }

            public string Day { get; set; }

            
        }
         public static void Main()
        {
            var factory = new ConnectionFactory();

            factory.UserName = "guest";

            factory.Password = "guest";

            factory.VirtualHost = "/";

            factory.HostName = "localhost";

            IConnection conn = factory.CreateConnection();

            IModel channel = conn.CreateModel();

            var queueName = "hiwWQ3";

            var exchange = "";

            var routingKey = "";


            channel.QueueDeclare(queueName, true, false, false, null);


            var command = new AddData
            {
                Username = "Angular",

                Day = "3",

                Shipping = "400"
            };

            var properties = channel.CreateBasicProperties();

            properties.Persistent = true;

            //string message = JsonConvert.SerializeObject(command);

            //var body = Encoding.UTF8.GetBytes(message);

            //channel.BasicPublish(exchange: "", routingKey: "hiwWQ2", basicProperties: properties, body: body);

            Send.main(command , exchange , routingKey , channel , properties);

            channel.Dispose();

            channel.Close();
        }
    }
}
