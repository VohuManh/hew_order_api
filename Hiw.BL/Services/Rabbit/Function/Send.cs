﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using Newtonsoft.Json;

namespace Hiw.BL.Services.Rabbit.Function
{
   public class Send
    {
        public static void main(object command, string exchange , string routingKey ,IModel channel , IBasicProperties properties )
        {
            string message = JsonConvert.SerializeObject(command);

            var body = Encoding.UTF8.GetBytes(message);

            channel.BasicPublish(exchange: "", routingKey: "hiwWQ2", basicProperties: properties, body: body);
        }
    }
}
