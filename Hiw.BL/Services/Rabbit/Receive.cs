﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Hiw.BL.Services.Rabbit
{
   public class Receive
    {
        public static string message;
        public static string Main()
        {
            var factory = new ConnectionFactory();

            factory.HostName = "localhost";

            factory.UserName = "guest";

            factory.Password = "guest";

            factory.VirtualHost = "/";

            IConnection conn = factory.CreateConnection();

            IModel channel = conn.CreateModel();

            

            var queueName = "hiWhiW";

            channel.QueueDeclare(queueName, true, false, false, null);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;

                message = Encoding.UTF8.GetString(body);
            };

            channel.BasicConsume(queueName, false, consumer);


            return message;
        }
    }
}
