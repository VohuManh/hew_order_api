﻿using Hiw.BL.Entities.Su;
using SS.Data.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace Hiw.BL.Services
{
    public static class SystemConfigurationService
    {
        private static void LoadData()
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.Priority = CacheItemPriority.NotRemovable;

            IList<SuSystemConfiguration> configs = ServiceProvider.SuSystemConfigurationService.FindAll(false).ToList();
            foreach (SuSystemConfiguration config in configs)
            {
                CacheProvider.ConfigurationCache.Add(string.Format("{0}-{1}", config.ConfigurationGroupCode, config.ConfigurationName), config.ConfigurationValue, policy);
            }
        }

        private static string GetValue(string configurationGroupCode, string configurationName)
        {
            if (CacheProvider.ConfigurationCache.Count() == 0)
            {
                LoadData();
            }

            if (!CacheProvider.ConfigurationCache.Contains(string.Format("{0}-{1}", configurationGroupCode, configurationName)))
            {
                // Fetch data by configuration name on demand.
                SuSystemConfiguration configuration = new SuSystemConfiguration();
                configuration.ConfigurationGroupCode = configurationGroupCode;
                configuration.ConfigurationName = configurationName;
                configuration = ServiceProvider.SuSystemConfigurationService.FindByCriterias(new SuSystemConfiguration() { ConfigurationGroupCode = configurationGroupCode, ConfigurationName = configurationName }, false, new string[] { "ConfigurationGroupCode", "ConfigurationName" }).FirstOrDefault();
                if (configuration != null)
                {
                    CacheItemPolicy policy = new CacheItemPolicy();
                    policy.Priority = CacheItemPriority.NotRemovable;
                    CacheProvider.ConfigurationCache.Set(string.Format("{0}-{1}", configuration.ConfigurationGroupCode, configuration.ConfigurationName), configuration.ConfigurationValue, policy);
                    return configuration.ConfigurationValue;
                }
            }
            else
            {
                return CacheProvider.ConfigurationCache.Get(string.Format("{0}-{1}", configurationGroupCode, configurationName)).ToString();
            }

            return null;
        }

        public static T GetConfiguration<T>(string configurationGroupCode, string configurationName)
        {
            string configurationValue = GetValue(configurationGroupCode, configurationName);
            if (configurationValue != null)
            {
                return (T)TypeExtension.ChangeType(configurationValue, typeof(T));
            }
            else
            {
                return default(T);
            }
        }

        public static string GetConfiguration(string configurationGroupCode, string configurationName)
        {
            return GetValue(configurationGroupCode, configurationName);
        }

        public static void ClearParameter(string configurationGroupCode, string configurationName)
        {
            CacheProvider.ConfigurationCache.Remove(string.Format("{0}-{1}", configurationGroupCode, configurationName));
        }

        public static void Neologize()
        {
            CacheProvider.ConfigurationCache.Dispose();
        }


    }
}
