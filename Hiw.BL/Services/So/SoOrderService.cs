﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.So;
using Hiw.BL.Services;
using Dapper;


namespace Hiw_Order.BL.Services.So
{
   public class SoOrderService : ServiceBase<SoOrder>
    {
        public SoOrderService(string connectionstring) : base(connectionstring)
        {

        }

        public int InsertOrder(SoOrder soOrder)
        {
            return this.Insert<int>(soOrder);
        }

        public Boolean DeleteOrder(SoOrder delete)
        {
            SoOrder soOrder = new SoOrder();

            soOrder.OrderID = delete.OrderID;

            soOrder = ServiceProvider.SoOrderService.FindByKeys(soOrder);

            if(soOrder == null)
            {
                return false;
            }
            else
            {
               
                this.Delete(soOrder);
                return true;
            }
        }

        public SoOrder GetOrder(int orderID)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dbParam = new DynamicParameters();

            sql.AppendLine(@"select * from SoOrder where OrderID = @orderID");

            dbParam.Add("orderID", orderID);

            return this.ExecuteUniqueQuery<SoOrder>(sql.ToString(), dbParam);
        }

        public SoOrder UpdateStatus(SoOrder previous , int status , out string message)
        {
            SoOrder soOrder = new SoOrder();

            soOrder.OrderID = previous.OrderID;

            soOrder = ServiceProvider.SoOrderService.FindByKeys(soOrder);

            if(soOrder == null)
            {
                message = "Can not find the Order with OrderID : " + soOrder.OrderID.ToString();

                return previous;
            }
            else
            {
                message = "Update Complete";

                soOrder.StatusID = status;

                this.Update(soOrder);

                return GetOrder(soOrder.OrderID);

            }
        }
    }
}
