﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.So;
using Hiw.BL.Services;
using Dapper;

namespace Hiw_Order.BL.Services.So
{
   public class SoBankService : ServiceBase<SoBank>
    {
        public SoBankService (string connectionstring) : base(connectionstring)
        {

        }

        public IEnumerable<SoBank> GetBank()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(@"select * from SoBank ");

            return this.ExecuteQuery<SoBank>(sql.ToString());

            
        }

        public SoBank update(SoBank update , out string errorMessage)
        {
            SoBank soBank = new SoBank();

            soBank.BankID = update.BankID;

            soBank = ServiceProvider.SoBankService.FindByKeys(soBank);

            if(soBank == null)
            {
                errorMessage = "Can not find the Bank when ID is " + update.BankID.ToString();

                return soBank;
            }else
            {
                errorMessage = "None of Error";

                soBank.BankName = update.BankName;

                soBank.BankAccountNumber = update.BankAccountNumber;

                soBank.BankAccountName = update.BankAccountName;

                return soBank;
            }
        }

        public int InsertBank(SoBank soBank)
        {
            return this.Insert<int>(soBank);
        }

        public void DeleteBank(int id)
        {
            SoBank soBank = new SoBank();

            soBank.BankID = id;

            soBank = ServiceProvider.SoBankService.FindByKeys(soBank);

            this.Delete(soBank);
        }
    }
}
