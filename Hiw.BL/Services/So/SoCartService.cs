﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.So;
using Hiw.BL.Services;
using Dapper;

namespace Hiw_Order.BL.Services.So
{
   public class SoCartService : ServiceBase<SoCart>
    {
        public SoCartService (string connectionstring) : base(connectionstring)
        {

        }

        public int InsertCart(SoCart soCart)
        {
            return this.Insert<int>(soCart);
        }

        public void DeleteAllCart(int userID)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dbParams = new DynamicParameters();

            sql.AppendLine(@"delete from SoCart where UserID = @userID");

            dbParams.Add("userID", userID);

            this.ExecuteNonQuery(sql.ToString(), dbParams);

        }
    }
}
