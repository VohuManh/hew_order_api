﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.So;
using Dapper;

namespace Hiw_Order.BL.Services.So
{
    public class SoOrderDetailsService : ServiceBase<SoOrderDetails>
    {
        public SoOrderDetailsService(string connectionstring) : base(connectionstring)
        {

        }

        public IEnumerable<SoOrderDetails> GetOrderDetails(int orderID)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dynamicParameters = new DynamicParameters();

            sql.AppendLine(@"select * from SoOrderDetails where OrderID = @orderID");

            dynamicParameters.Add("orderID", orderID);

            return this.ExecuteQuery<SoOrderDetails>(sql.ToString(), dynamicParameters);
        }

        public List<int> InsertOrderDetails(List<SoOrderDetails> soOrderDetails, int orderID)
        {
            var i = 0;

            List<int> list = new List<int>();

            while (i < soOrderDetails.Count)
            {
                soOrderDetails[i].OrderID = orderID;

                list.Add(this.Insert<int>(soOrderDetails[i]));

                i++;
            }

            return list;
        }



    }
}
