﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.So;
using Hiw.BL.Services;

namespace Hiw_Order.BL.Services.So
{
   public class SoPaymentOrderService : ServiceBase<SoPaymentOrder>
    {
        public SoPaymentOrderService(string connectionstring) : base(connectionstring)
        {

        }

        public int InsertPayment(SoPaymentOrder soPaymentOrder)
        {
            return this.Insert<int>(soPaymentOrder);
        }

        public void DeletePayment(int paymentID)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dbParams = new DynamicParameters();

            sql.AppendLine(@"delete from SoPaymentOrder where PaymentID = @paymentid");

            dbParams.Add("paymentid", paymentID);

            this.ExecuteNonQuery(sql.ToString(), dbParams);
        }

        public SoPaymentOrder GetPayment(int paymentID)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dbParams = new DynamicParameters();

            sql.AppendLine(@"select * from SoPaymentOrder where PaymentID = @paymentid");

            dbParams.Add("paymentid", paymentID);

            return this.ExecuteUniqueQuery<SoPaymentOrder>(sql.ToString(), dbParams);
        }

        public void GetAllPayment(int userid)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dbParams = new DynamicParameters();

            sql.AppendLine(@"select PaymentID , SoPaymentOrder.CreatedDate from SoPaymentOrder inner join SoOrder on SoOrder.OrderID = SoPaymentOrder.OrderID where SoOrder.UserID = @userid ");

            dbParams.Add("userid", userid);

            //return this.ExecuteQuery<>(sql.ToString(), dbParams);
        }
    }
}
