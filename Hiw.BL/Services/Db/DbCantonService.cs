﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.Db;
using Dapper;

namespace Hiw_Order.BL.Services.Db
{
   public class DbCantonService : ServiceBase<DbCanton>
    {
        public DbCantonService (string connectionstring) : base(connectionstring)
        {

        }

        public IEnumerable<DbCanton> GetCanton(int districtCode)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dynamicParameters = new DynamicParameters();

            sql.AppendLine(@"select * from DbCanton where DistrictCode = @districtcode");

            dynamicParameters.Add("districtcode", districtCode);

            return this.ExecuteQuery<DbCanton>(sql.ToString(), dynamicParameters);
            
        }

        public void InsertCanton(DbCanton dbCanton)
        {
            this.Insert(dbCanton);
        }
    }
}
