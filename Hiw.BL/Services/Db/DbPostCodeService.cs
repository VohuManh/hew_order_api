﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.Db;
using Dapper;

namespace Hiw_Order.BL.Services.Db
{
    public class DbPostCodeService : ServiceBase<DbPostCode>
    {
        public DbPostCodeService(string connectionstring) : base(connectionstring)
        {

        }
        
        public IEnumerable<DbPostCode> GetPostCode(int cantonCode)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dynamicParameters = new DynamicParameters();

            sql.AppendLine(@"select * from DbPostCode where CantonCode = @cantoncode");

            dynamicParameters.Add("cantoncode", cantonCode);

            return this.ExecuteQuery<DbPostCode>(sql.ToString(), dynamicParameters);
        }

        public void InsertPostCode(DbPostCode dbPostCode)
        {
            this.Insert(dbPostCode);
        }
    }
}
