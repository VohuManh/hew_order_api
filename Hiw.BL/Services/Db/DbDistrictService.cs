﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.Db;
using Dapper;

namespace Hiw_Order.BL.Services.Db
{
   public class DbDistrictService : ServiceBase<DbDistrict>
    {
        public DbDistrictService(string connectionstring) : base(connectionstring)
        {

        }

        public IEnumerable<DbDistrict> GetDistrict(int provinceCode)
        {
            DynamicParameters dynamicParameters = new DynamicParameters();

            StringBuilder sql = new StringBuilder();

            sql.AppendLine(@"select * from DbDistrict where ProvinceCode = @provinceCode");

            dynamicParameters.Add("provinceCode", provinceCode);

            return this.ExecuteQuery<DbDistrict>(sql.ToString(), dynamicParameters);
        }

        public void InsertDistrict(DbDistrict dbDistrict)
        {
            this.Insert(dbDistrict);
        }
    }
}
