﻿using System;
using System.Collections.Generic;
using System.Text;
using Hiw_Order.BL.Entities.Db;
using SS.Data.SqlServer;

namespace Hiw_Order.BL.Services.Db
{
   public class DbProvinceService : ServiceBase<DbProvince>
    {
        public DbProvinceService (string connectionstring) : base(connectionstring)
        {

        }

        public IEnumerable<DbProvince> GetProvinces()
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendLine(@"select * from DbProvince");

            return this.ExecuteQuery<DbProvince>(sql.ToString());
        }

        public void InsertProvince(DbProvince dbProvince)
        {
            this.Insert(dbProvince);
        }
    }
}
