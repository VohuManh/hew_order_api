﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Data.SqlServer;
using Hiw_Order.BL.Entities.Su;
using Dapper;
using Hiw.BL.Services;


namespace Hiw_Order.BL.Services.Su
{
   public class SuAddressService : ServiceBase<SuAddress>
    {
        public SuAddressService(string connectionstring) : base(connectionstring)
        {

        }

        public int InsertAddress(SuAddress suAddress)
        {
            return this.Insert<int>(suAddress);
        }

        public IEnumerable<SuAddress> GetAllAddress(int userID)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dynamicParameters = new DynamicParameters();

            sql.AppendLine(@"select * from SuAddress where UserID = @userId");

            dynamicParameters.Add("userId", userID);

            return this.ExecuteQuery<SuAddress>(sql.ToString(), dynamicParameters);
        }

        public SuAddress GetAddress(int AddressID)
        {
            StringBuilder sql = new StringBuilder();

            DynamicParameters dynamicParameters = new DynamicParameters();

            sql.AppendLine(@"select * from SuAddress where AddressID = @AddressID");

            dynamicParameters.Add("userId", AddressID);

            return this.ExecuteUniqueQuery<SuAddress>(sql.ToString(), dynamicParameters);
        }

        public SuAddress UpdateAddress(SuAddress updateValue , out string ErrorMessage)
        {
            SuAddress suAddress = new SuAddress();

            suAddress.AddressID = updateValue.AddressID;

            suAddress = ServiceProvider.SuAddressService.FindByKeys(suAddress);

            if(suAddress == null)
            {
                ErrorMessage = "Cannot Find UserID " + suAddress.AddressID.ToString();

                return suAddress;
            }
            else
            {
                suAddress.Address = updateValue.Address;
                suAddress.FirstName = updateValue.FirstName;
                suAddress.LastName = updateValue.LastName;
                suAddress.PostCode = updateValue.PostCode;
                suAddress.CantonCode = updateValue.CantonCode;
                suAddress.DistrictCode = updateValue.DistrictCode;
                suAddress.ProvinceCode = updateValue.ProvinceCode;
                suAddress.MobilePhoneNo = updateValue.MobilePhoneNo;

                this.Update(suAddress);

                ErrorMessage = null;

                return GetAddress(suAddress.AddressID);
            }
        }
    }

}
