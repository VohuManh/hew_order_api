﻿using Hiw.BL.Entities.Su;
using SS.Data.SqlServer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hiw.BL.Services.Su
{
    public class SuSystemConfigurationService : ServiceBase<SuSystemConfiguration>
    {
        public SuSystemConfigurationService(string connectionString) : base(connectionString) { }
    }
}
