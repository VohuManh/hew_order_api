﻿
using Hiw.BL.Services.Su;
using SS.Web.API.Utilities;
using System;
using System.Collections.Generic;
using System.Text;
using Hiw_Order.BL.Services.Su;
using Hiw_Order.BL.Services.So;
using Hiw_Order.BL.Services.Db;

namespace Hiw.BL.Services
{
    public static class ServiceProvider
    {
        private static string ConnectionStrings { get { return ConfigurationHelper.Configuration.GetSection("ConnectionStrings:DefaultConnection").Value; } }
        public static Microsoft.Extensions.DependencyInjection.ServiceProvider Provider { get; set; }

        public static IUserAccount UserAccountService
        {
            get
            {
                return Provider.GetService(typeof(IUserAccount)) as IUserAccount;
            }
        }

        public static IFormData FormDataService
        {
            get
            {
                return Provider.GetService(typeof(IFormData)) as IFormData;
            }
        }

        private static SuSystemConfigurationService suSystemConfigurationService;
        public static SuSystemConfigurationService SuSystemConfigurationService
        {
            get
            {
                if (suSystemConfigurationService == null)
                {
                    suSystemConfigurationService = new SuSystemConfigurationService(ConnectionStrings);
                }
                return suSystemConfigurationService;
            }
        }

        private static SoOrderService soOrderService;

        public static SoOrderService SoOrderService
        {
            get
            {
                if(soOrderService == null)
                {
                    soOrderService = new SoOrderService(ConnectionStrings);
                }

                return soOrderService;
            }
        }

        private static SuAddressService suAddressService;

        public static SuAddressService SuAddressService
        {
            get
            {
                if(suAddressService == null)
                {
                    suAddressService = new SuAddressService(ConnectionStrings);
                }

                return suAddressService;
            }
        }

        private static DbDistrictService dbDistrictService;

        public static DbDistrictService DbDistrictService
        {
            get
            {
                if(dbDistrictService == null)
                {
                    dbDistrictService = new DbDistrictService(ConnectionStrings);
                }

                return dbDistrictService;
            }
        }


        private static DbCantonService dbCantonService;

        public static DbCantonService DbCantonService
        {
            get
            {
                if (dbCantonService == null)
                {
                    dbCantonService = new DbCantonService(ConnectionStrings);
                }

                return dbCantonService;
            }
        }


        private static DbPostCodeService dbPostCodeService;

        public static DbPostCodeService DbPostCodeService
        {
            get
            {
                if (dbPostCodeService == null)
                {
                    dbPostCodeService = new DbPostCodeService(ConnectionStrings);
                }

                return dbPostCodeService;
            }
        }


        private static DbProvinceService dbProvinceService;

        public static DbProvinceService DbProvinceService
        {
            get
            {
                if (dbProvinceService == null)
                {
                    dbProvinceService = new DbProvinceService(ConnectionStrings);
                }

                return dbProvinceService;
            }
        }

        private static SoOrderDetailsService soOrderDetailsService;

        public static SoOrderDetailsService SoOrderDetailsService
        {
            get
            {
                if(soOrderDetailsService == null)
                {
                    soOrderDetailsService = new SoOrderDetailsService(ConnectionStrings);
                }

                return soOrderDetailsService;
            }
        }

        private static SoBankService soBankService;

        public static SoBankService SoBankService
        {
            get
            {
                if (soBankService == null)
                {
                    soBankService = new SoBankService(ConnectionStrings);
                }

                return soBankService;
            }
        }

        private static SoCartService soCartService;
        public static SoCartService SoCartService
        {
            get
            {
                if (soCartService == null)
                {
                    soCartService = new SoCartService(ConnectionStrings);
                }

                return soCartService;
            }
        }

        private static SoPaymentOrderService soPaymentOrderService;
        public static SoPaymentOrderService SoPaymentOrderService
        {
            get
            {
                if (soPaymentOrderService == null)
                {
                    soPaymentOrderService = new SoPaymentOrderService(ConnectionStrings);
                }

                return soPaymentOrderService;
            }
        }
    }
}
