﻿using SS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hiw.BL.Entities.Su
{
    [EntityMapping(TableMapping = "SuSystemConfiguration")]
    public class SuSystemConfiguration : EntityBase
    {
        [EntityScalarProperty(EntityKey = true, IdentityKey = true)]
        public int Id { get; set; }
        public string ConfigurationGroupCode { get; set; }
        public string ConfigurationName { get; set; }
        public string ConfigurationValue { get; set; }
        public int? Sequence { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
    }
}
