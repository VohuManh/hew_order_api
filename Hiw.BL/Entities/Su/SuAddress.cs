﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.Su
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.Su.SuAddress , Hiw_Order.BL" , TableMapping = "SuAddress")]
   public class SuAddress : EntityBase
    {
        [EntityScalarProperty(EntityKey = true , IdentityKey = true)]
        
        public int AddressID { get; set; }

        public int UserID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MobilePhoneNo { get; set; }

        public string Address { get; set; }

        public int CantonCode { get; set; }

        public int DistrictCode { get; set; }

        public int ProvinceCode { get; set; }

        public int PostCode { get; set; }
    }
}
