﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;
namespace Hiw_Order.BL.Entities.So
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.So.SoOrder , Hiw_Order.BL", TableMapping = "SoOrder")]
    public class SoOrder : EntityBase
    {
        [EntityScalarProperty(EntityKey = true, IdentityKey = true)]

        public int OrderID { get; set; }

        public int UserID { get; set; }

        public int AddressID {get; set;}

        public int StatusID { get; set; }

        public string PaymentOption { get; set; }

        public int SubTotal { get; set; }

        public int ShoppingFee { get; set; }

        public int ServiceFee { get; set; }

        public int Total { get; set; }
    }
}
