﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.So
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.So.SoBank , Hiw_Order.BL" , TableMapping = "SoBank")]
   public class SoBank : EntityBase
    {
        [EntityScalarProperty(EntityKey = true , IdentityKey = true)]

        public int BankID { get; set; }

        public string BankName { get; set; }

        public string BankAccountName { get; set; }
        
        public string BankAccountNumber { get; set; }

    }
}
