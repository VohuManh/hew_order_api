﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.So
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.So.SoPaymentOrder , Hiw_Order.BL" , TableMapping = "SoPaymentOrder")]
   public class SoPaymentOrder : EntityBase
    {
        [EntityScalarProperty(EntityKey = true , IdentityKey = true)]

        public int PaymentID { get; set; }

        public int OrderID { get; set; }

        public int BankID { get; set; }

        public int ShippingPrice { get; set; }

        public string PaymentCode { get; set; }

        public int PayCount { get; set; }

        public int PaymentAmount { get; set; }

        public string Currency { get; set; }

        public DateTime TransferPaymentDateTime { get; set; }

        public string EvidenceUrl { get; set; }

    }
}
