﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.So
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.So.SoStatus , Hiw_Order.BL ", TableMapping = "SoStatus")]
    public class SoStatus : EntityBase
    {
        [EntityScalarProperty(EntityKey = true, IdentityKey = true)]

        public int StatusID { get; set; }

        public string Description { get; set; }

    }
}
