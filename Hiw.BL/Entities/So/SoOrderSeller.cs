﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.So
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.So.SoOrderSeller , Hiw_Order.BL", TableMapping = "SoOrderSeller")]
    public class SoOrderSeller
    {
        [EntityScalarProperty(EntityKey = true, IdentityKey = true)]

        public int SellerID { get; set; }

        public int HiwerID { get; set; }

        public int OrderID { get; set; }

        public string ShippingOption { get; set; }

        public string ShippingFee { get; set; }

    }
}
