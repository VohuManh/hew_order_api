﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.So
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.So.SoOrderDetails , Hiw_Order.BL", TableMapping = "SoOrderDetails")]
   public class SoOrderDetails : EntityBase
    {
        [EntityScalarProperty(EntityKey = true, IdentityKey = true)]

        public int ID { get; set; }

        public string ProductID { get; set; }

        public int SellerID { get; set; }

        public int OrderID { get; set; }

        public int ProductPrice { get; set; }

        public int Quantity { get; set; }

        public int ServicePrice { get; set; }

        public int Total { get; set; }


    }
}
