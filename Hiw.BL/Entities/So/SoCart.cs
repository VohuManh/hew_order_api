﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.So
{
    [EntityMapping(EntityTypeName ="Hiw_Order.BL.Entities.So.SoCart , Hiw_Order.BL" , TableMapping = "SoCart")]
   public class SoCart : EntityBase
    {
        [EntityScalarProperty(EntityKey = true , IdentityKey = true)]

        public int CartID { get; set; }

        public int UserID { get; set; }

        public int ProductID { get; set; }

        public int Quantity { get; set; }

    }
}
