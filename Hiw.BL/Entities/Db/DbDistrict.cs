﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.Db
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.Db.DbDistrict , Hiw_Order.BL" , TableMapping = "DbDistrict")]
   public class DbDistrict : EntityBase
    {
        [EntityScalarProperty(EntityKey = true , IdentityKey = true)]

        public int DistrictCode { get; set; }

        public string DistrictName { get; set; }

        public int ProvinceCode { get; set; }

    }
}
