﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.Db
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.Db.DbProvince , Hiw_Order.BL" , TableMapping = "DbProvince")]
    public class DbProvince : EntityBase
    {
        [EntityScalarProperty(EntityKey = true, IdentityKey = true)]

        public int ProvinceCode { get; set; }

        public string ProvinceName { get; set; }
    }
}
