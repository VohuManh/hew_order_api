﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.Db
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.Db.DbCanton , Hiw_Order.BL ", TableMapping = "DbCanton")]
    public class DbCanton : EntityBase
    {
        [EntityScalarProperty(EntityKey = true, IdentityKey = true)]
        public int CantonCode { get; set; }

        public string CantonName { get; set; }

        public int ProvinceCode { get; set; }

        public int DistrictCode { get; set; }
    }
}
