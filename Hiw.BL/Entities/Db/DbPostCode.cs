﻿using System;
using System.Collections.Generic;
using System.Text;
using SS.Core.Entities;

namespace Hiw_Order.BL.Entities.Db
{
    [EntityMapping(EntityTypeName = "Hiw_Order.BL.Entities.Db.DbPostcode , Hiw_Order.BL ", TableMapping = "DbPostCode")]
    public class DbPostCode : EntityBase
    {
        [EntityScalarProperty(EntityKey = true, IdentityKey = true)]

        public int CantonCode { get; set; }

        public int PostCode { get; set; }

        public int DistrictCode { get; set; }

        public int ProvinceCode { get; set; }
    }
}
