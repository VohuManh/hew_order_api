﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Hiw.BL
{
    public static class ConfigurationHelper
    {
        public static IConfiguration Configuration
        {
            get
            {
                return GetConfig();
            }
        }

        private static IConfiguration GetConfig()
        {
            IConfiguration configuration = new ConfigurationBuilder()
                                                .SetBasePath(Directory.GetCurrentDirectory()) // Directory where the json files are located
                                                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                                                .Build();
            return configuration;
        }
    }
}
